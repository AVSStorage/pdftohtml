<?php
// if you are using composer, just use this
include 'vendor/autoload.php';

// change pdftohtml bin location
\Gufy\PdfToHtml\Config::set('pdftohtml.bin', '/usr/bin/pdftohtml');

// change pdfinfo bin location
\Gufy\PdfToHtml\Config::set('pdfinfo.bin', '/usr/bin/pdfinfo');

// initiate

$pdf = new Gufy\PdfToHtml\Pdf('t3hyt.pdf');

// convert to html string
$html = $pdf->html();
// print_r($html);
// print_r($pdf->html(2));

$file = 'page1.html';

// Write the contents back to the file
file_put_contents($file, $html);

$file = 'page2.html';
file_put_contents($file, $pdf->html(2));
   
// convert a specific page to html string
// $page = $pdf->html(3);

// convert to html and return it as [Dom Object](https://github.com/paquettg/php-html-parser)
// $dom = $pdf->getDom();

// check if your pdf has more than one pages
// $total_pages = $pdf->getPages();

// Your pdf happen to have more than one pages and you want to go another page? Got it. use this command to change the current page to page 3
// $dom->goToPage(3);

// and then you can do as you please with that dom, you can find any element you want
// $paragraphs = $dom->find('body > p');



?>
